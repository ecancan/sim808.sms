#include <DFRobot_sim808.h>
#include <sim808.h>
#include <SoftwareSerial.h>
#include <string.h>
#define MESSAGE_LENGTH 160
char message[MESSAGE_LENGTH];
int messageIndex = 0;
char* sendMsg;
String messageConvert;

char phone[16];
char datetime[24];

#define PIN_TX 10
#define PIN_RX 11
SoftwareSerial mySerial(PIN_TX,PIN_RX);
DFRobot_SIM808 sim808(&mySerial);

void sendSMS();
void readSMS();

void setup()
{
  mySerial.begin(9600);
  Serial.begin(9600);
  while(!sim808.init())
  {
      Serial.print("Hata! Sim Okunamadı\r\n");
      delay(1000);
  }
  delay(3000);
  Serial.println("Sim Tanımlandı");
  Serial.println("Mesaj Alımı Açıktır.");
}

void loop()
{
   messageIndex = sim808.isSMSunread();
   if (messageIndex > 0)
   { 
      readSMS();
      sendSMS();
      sim808.detachGPS();
      Serial.println("Mesaj Alımı Açık");
   }
}

void readSMS()
{
  Serial.print("messageIndex: ");
  Serial.println(messageIndex);
  sim808.readSMS(messageIndex, message, MESSAGE_LENGTH, phone, datetime);
  sim808.deleteSMS(messageIndex);
  Serial.print("Mesajı Gönderen Numara: ");
  Serial.println(phone);  
  Serial.print("Tarih: ");
  Serial.println(datetime);        
  Serial.print("Mesaj İçeriği: ");
  Serial.println(message);
}


void sendSMS()
{
  Serial.println("Mesaj Gönderiliyor...");
  Serial.println(phone);
  messageConvert = String(message);
  if(messageConvert == "Merhaba"){
    sendMsg = "Nasilsin?";
  }else{
    sendMsg = "Tanimlanan bir komut degil?";
  }
  sim808.sendSMS(phone,sendMsg);
}

